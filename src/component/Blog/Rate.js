import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { withRouter } from 'react-router';
import StarRatings from 'react-star-ratings'
class Rate extends Component{
    constructor(props){
      super(props)
      this.state = {
        rate:[]
      }
      this.changeRating = this.changeRating.bind(this);
      this.componentDidMount = this.componentDidMount.bind(this);

  }

componentDidMount(){
    let urlget = 'http://localhost/laravel/laravel/public/api/blog/rate/' + this.props.match.params.id
    axios.get(urlget)
    .then(res1 => {
      // console.log(res1.data.data)
      console.log(res1.data)
      let arr = res1.data.data
      let s=0
      let i=0
      if (arr instanceof Array){
        arr.map((value, index) => {
          console.log(value.rate)
          s = s + value.rate
          i = i + 1
        })
      }
      if(arr instanceof Object){
        Object.keys(arr).map((key, index) => {
          console.log(arr[key].rate)
          s = s + arr[key].rate
          i = i + 1
        })
      }
      let tb = s/i
      console.log(s, i, tb)
      this.setState({
        rating: tb
      });
    })
    .catch(error => console.log(error));
  }
  
changeRating( newRating, name ) {

    this.setState({
      rating: newRating
    });

    console.log(newRating)

    var getlocal = localStorage.getItem("a")
    if(getlocal) {
        var index=JSON.parse(getlocal);
            if (index.islogin != true){
              this.props.history.push('/login')
              }
          }
    let items = this.props.items
    let blog_id = items.id
    let user_id = index.ob['Auth']['id']

 

    let accessToken = index.ob['success']['token']  
    
    let config = {                
    headers: {                
    'Authorization': 'Bearer '+ accessToken,                
    'Content-Type': 'application/x-www-form-urlencoded',                
    'Accept': 'application/json'                
    }                
    }; 

    let url='http://localhost/laravel/laravel/public/api/blog/rate/'+blog_id           
    const formData = new FormData();                
    formData.append('user_id', user_id);                
    formData.append('blog_id', blog_id);                
    formData.append('rate', newRating);                
    axios.post(url, formData, config)
    .then(res => {
      if(res){
        // console.log(res)
      }
      else{
        console.log(res.data.errors)
      }
    })
    .catch(error => console.log(error));

}



render(){
  return(
<div>
    <StarRatings
      rating={this.state.rating}
      starRatedColor="yellow"
      changeRating={this.changeRating}
      numberOfStars={5}
      name='rating'
    />
</div>
  );
  }
    
}
export default withRouter(Rate);