import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Header from "../Layout/Header";
import Detail from "./Detail";
import { withRouter } from 'react-router';
import Listcomment from "./Listcomment";

class Comment extends Component{
    constructor(props){
      super(props)
      this.state = {
        content: '',
        items: [],
        cmt: [],
        id_comment:''
      
      }
      this.submitpost = this.submitpost.bind(this);
      this.showcontent = this.showcontent.bind(this);
  }
showcontent(e){
    this.setState({
    content: e.target.value
    })
 }
submitpost(e){
  e.preventDefault();
  var getlocal = localStorage.getItem("a")
    if(getlocal) {
      var index=JSON.parse(getlocal);
		  if (index.islogin != true){
				return(
          this.props.history.push('/login')
				)
			}
		}	
  
  let items = this.props.items
  let id_comment = this.props.id_comment
  console.log(id_comment)
  let id_blog = items.id

  let id_user = index.ob['Auth']['id']
  let name_user = index.ob['Auth']['name']
  let content = this.state.content
  let image_user = index.ob['Auth']['avatar']
  let url = 'http://localhost/laravel/laravel/public/api/blog/comment/' + id_blog                
  let accessToken = index.ob['success']['token']  
  let config = {                
  headers: {                
  'Authorization': 'Bearer '+ accessToken,                
  'Content-Type': 'application/x-www-form-urlencoded',                
  'Accept': 'application/json'                
  }                
  };                
  let comment = this.state.content     
  if(comment) {                
  const formData = new FormData();                
  formData.append('id_blog', id_blog);                
  formData.append('id_user', id_user);                
  formData.append('id_comment', this.props.id_comment ? this.props.id_comment : 0);                
  formData.append('comment', content);                
  formData.append('image_user', image_user);                
  formData.append('name_user', name_user); 

  axios.post(url, formData, config)
  .then(res => {
      if(res.data){
        // console.log(res.data.data)
        this.props.getCmt(res.data.data)
      }
      else{
        console.log(res.data.errors)
      }
  })
  .catch(error => console.log(error));
  }
}
render(){
    return(
        <div>
              <div className="replay-box">
                <div className="row">
                  <div className="col-sm-12">
                    <h2>Leave a replay</h2>
                    <form onSubmit={this.submitpost}>
                      <div className="text-area">
                        <div className="blank-arrow">
                          
                            <label>Your Name</label>
                          </div>
                          <span>*</span>
                          <textarea name="message" rows={11} Value={this.state.content} onChange={this.showcontent} />
                          <button type="submit" className="btn btn-primary">Post comment</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>{/*/Repaly Box*/}
        </div>
    )
    }
}
export default withRouter(Comment);
