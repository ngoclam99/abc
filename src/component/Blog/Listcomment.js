import React, { Component } from "react";
import Comment from "./Comment";
import { Link } from "react-router-dom";
import { withRouter } from 'react-router';

class Listcomment extends Component{
    constructor(props){
      super(props)
      this.state = {
        items:[],
      
      }
      this.showcmt = this.showcmt.bind(this)
      this.reply = this.reply.bind(this)
  }
reply(e){

    let id_comment = e.target.id
    this.props.Rep(id_comment)
   
}
showcmt(){
    let cmt = this.props.cmt
    // console.log(cmt)

    if (cmt.length > 0 ){
      return cmt.map((value, index) => {
        if(value.id_comment == 0){
          return(
            <React.Fragment key={index}>
              <li className="media">
                  <Link className="pull-left" to="#">
                    <img className="media-object" src={"http://localhost/laravel/laravel/public/upload/user/avatar/"+value.image_user} alt="" />
                  </Link>
                  <div className="media-body">
                      <ul className="sinlge-post-meta">
                        <li><i className="fa fa-user" />{value.name_user}</li>
                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                      </ul>
                      <p>{value.comment}</p>
                      <Link className="btn btn-primary" id={value.id} onClick={this.reply} ><i className="fa fa-reply" />Reply</Link>
                  </div>
                </li>

               {cmt.map((cmt1, j) => {
                if(value.id == cmt1.id_comment) {
                  return(
                      <li key={j} index={j} class="media second-media">
                          <a class="pull-left" href="#">
                            <img className="media-object" src={"http://localhost/laravel/laravel/public/upload/user/avatar/"+cmt1.image_user} alt="" />
                          </a>
                          <div class="media-body">
                            <ul class="sinlge-post-meta">
                              <li><i className="fa fa-user" />{cmt1.name_user}</li>
                              <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                              <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <p>{cmt1.comment}</p>
                            <Link className="btn btn-primary" id={cmt1.id} onClick={this.reply} ><i className="fa fa-reply" />Reply</Link>
                          </div>
                        </li>
                  )
                }
              }
              )
              }
            
            </React.Fragment>

          )
        }
      }
      )
  }
}

render(){
    return(
      <ul className="media-list">
            {this.showcmt()}
        </ul>
    )
    }
}
export default withRouter(Listcomment);