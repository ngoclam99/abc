import React, { Component, useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Comment from "./Comment";
import Listcomment from "./Listcomment";
import Rate from "./Rate";
function Detail2(){
  const [items ,setitems] = useState('')
  const [cmt ,setcmt] = useState('')
  const [id_comment ,setid_comment] = useState('')
  useEffect(() {
    let url = 'http://localhost/laravel/laravel/public/api/blog/detail/' + this.props.match.params.id
    axios.get(url)
    .then(res => {
        // console.log(res)
        const items=res.data.data;
        setitems(res.data.blog)
        setcmt(items["comment"])       
    })
    .catch(error => console.log(error));
  });

  function getCmt(data){
    console.log(data) 
    this.setState({cmt: this.state.cmt.concat(data)})
  }

  function Rep(id){
    // console.log(id)
    this.setState({id_comment: id})
  }
  
  function fetchData(){
    let items=this.state.items
    // console.log(items)
    // let cmt = this.state.cmt
    // console.log(cmt)
    // if (items.data instanceof Array){
        return(
            <div className="single-blog-post">
                  <h3>{items["title"]}</h3>
                  <div className="post-meta">
                    <ul>
                      <li><i className="fa fa-user" /> Mac Doe</li>
                      <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                      <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                    </ul>
                  </div>
                  <Link>
                    <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/"+ items["image"]} alt="image"/>
                  </Link>
                  <p>
                      {items["description"]}
                  </p>
                  <p>
                      {items["content"]}
                  </p>
                  <div className="pager-area">
                    <ul className="pager pull-right">
                      <li><Link to="#">Pre</Link></li>
                      <li><Link to="#">Next</Link></li>
                    </ul>
                  </div>
                </div>
        )
    // }
  }

  render(){
      return(
        <section>
        <div className="container">
          <div className="row">
            <div className="col-sm-3">
              <div className="left-sidebar">
                <h2>Category</h2>
                <div className="panel-group category-products" id="accordian">{/*category-productsr*/}
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <Link data-toggle="collapse" data-parent="#accordian" to="#sportswear">
                          <span className="badge pull-right"><i className="fa fa-plus" /></span>
                          Sportswear
                        </Link>
                      </h4>
                    </div>
                    <div id="sportswear" className="panel-collapse collapse">
                      <div className="panel-body">
                        <ul>
                          <li><Link >Nike </Link></li>
                          <li><Link >Under Armour </Link></li>
                          <li><Link >Adidas </Link></li>
                          <li><Link >Puma</Link></li>
                          <li><Link >ASICS </Link></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <Link data-toggle="collapse" data-parent="#accordian" to="#mens">
                          <span className="badge pull-right"><i className="fa fa-plus" /></span>
                          Mens
                        </Link>
                      </h4>
                    </div>
                    <div id="mens" className="panel-collapse collapse">
                      <div className="panel-body">
                        <ul>
                          <li><Link>Fendi</Link></li>
                          <li><Link>Guess</Link></li>
                          <li><Link >Valentino</Link></li>
                          <li><Link >Dior</Link></li>
                          <li><Link >Versace</Link></li>
                          <li><Link >Armani</Link></li>
                          <li><Link>Prada</Link></li>
                          <li><Link>Dolce and Gabbana</Link></li>
                          <li><Link>Chanel</Link></li>
                          <li><Link>Gucci</Link></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <Link data-toggle="collapse" data-parent="#accordian" to="#womens">
                          <span className="badge pull-right"><i className="fa fa-plus" /></span>
                          Womens
                        </Link>
                      </h4>
                    </div>
                    <div id="womens" className="panel-collapse collapse">
                      <div className="panel-body">
                        <ul>
                          <li><Link >Fendi</Link></li>
                          <li><Link >Guess</Link></li>
                          <li><Link >Valentino</Link></li>
                          <li><Link >Dior</Link></li>
                          <li><Link >Versace</Link></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title"><Link to="#">Kids</Link></h4>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title"><Link to="#">Fashion</Link></h4>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title"><Link to="#">Households</Link></h4>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title"><Link to="#">Interiors</Link></h4>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title"><Link to="#">Clothing</Link></h4>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title"><Link to="#">Bags</Link></h4>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title"><Link to="#">Shoes</Link></h4>
                    </div>
                  </div>
                </div>{/*/category-products*/}
                <div className="brands_products">{/*brands_products*/}
                  <h2>Brands</h2>
                  <div className="brands-name">
                    <ul className="nav nav-pills nav-stacked">
                      <li><Link > <span className="pull-right">(50)</span>Acne</Link></li>
                      <li><Link > <span className="pull-right">(56)</span>Grüne Erde</Link></li>
                      <li><Link > <span className="pull-right">(27)</span>Albiro</Link></li>
                      <li><Link > <span className="pull-right">(32)</span>Ronhill</Link></li>
                      <li><Link > <span className="pull-right">(5)</span>Oddmolly</Link></li>
                      <li><Link > <span className="pull-right">(9)</span>Boudestijn</Link></li>
                      <li><Link > <span className="pull-right">(4)</span>Rösch creative culture</Link></li>
                    </ul>
                  </div>
                </div>{/*/brands_products*/}
                <div className="price-range">{/*price-range*/}
                  <h2>Price Range</h2>
                  <div className="well">
                    <input type="text" className="span2" defaultValue data-slider-min={0} data-slider-max={600} data-slider-step={5} data-slider-value="[250,450]" id="sl2" /><br />
                    <b>$ 0</b> <b className="pull-right">$ 600</b>
                  </div>
                </div>{/*/price-range*/}
                <div className="shipping text-center">{/*shipping*/}
                  <img src="images/home/shipping.jpg" alt="" />
                </div>{/*/shipping*/}
              </div>
            </div>
            <div className="col-sm-9">
              <div className="blog-post-area">
                <h2 className="title text-center">Latest From our Blog</h2>
                
                {fetchData()}
              </div>{/*/blog-post-area*/}
              
              <Rate items={this.state.items} />

              <div className="socials-share">
                <a><img src="http://localhost:3000/images/blog/socials.png" alt="" /></a>
              </div>{/*/socials-share*/}

              <div className="response-area">
                <h2>RESPONSES</h2>
                  <Listcomment cmt={this.state.cmt} Rep = {this.Rep}/>
              </div>{/*/Response-area*/}
                  <Comment  getCmt = {this.getCmt} items={this.state.items} id_comment = {this.state.id_comment} />
              {/* <Comment /> */}
              
            </div>


          </div>
        </div>
      </section>
      );
  }

}
export default Detail2;