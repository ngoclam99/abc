import React, { Component } from "react";
import axios from "axios";
import Showcart from "./Showcart";
import { AppContext } from "./AppContext";

class Cart extends Component{
  static contextType = AppContext

	constructor(props) {
    super(props)
		this.state = {
        allproduct :'', 
        id: '',
        total: ''

    }
    this.fetchData = this.fetchData.bind(this)		
    this.Local = this.Local.bind(this)	
    this.Localtru = this.Localtru.bind(this)	
    this.Xoasp = this.Xoasp.bind(this)
    // this.tong = this.tong.bind(this)	

	}
componentDidMount(){
  let cart = {}
  var getLocal=localStorage.getItem("cart")
  if(getLocal) {
    cart=JSON.parse(getLocal);
    this.setState({
      cart : cart
      });
    let url = 'http://localhost/laravel/laravel/public/api/product/cart'
    axios.post(url, cart)
      .then(res => {
         if(res.data){
            // console.log(res.data.data)
            let allP =  res.data.data;
            let total = 0
            Object.keys(allP).map((key, index) => {
              total = total + (allP[key]['qty'] * allP[key]['price'])
            })

            
            this.setState({
              allproduct : allP,
              total: total
            });
            localStorage.setItem("total",JSON.stringify(total));
          }
          else{
             console.log(res.data.errors)
          }
      })
      .catch(error => console.log(error));
  }

}
Local(idproduct){
  console.log(idproduct)
  let cart = this.state.cart
  let total = this.state.total
  // console.log(cart)
  Object.keys(cart).map(function(key, index){
    console.log(cart[key])
    if (key == idproduct){
      cart[key] += 1;
    }
  })
  localStorage.setItem("cart",JSON.stringify(cart));
  
  let allproduct = this.state.allproduct
  Object.keys(allproduct).map((key, index) => {
    if(idproduct == allproduct[key]['id']){
      total = total + allproduct[key]['price']
    }
  })
  this.setState({
    total: total
  });
  // console.log(cart)
 
}
Localtru(idproduct){
  console.log(idproduct)
  let cart = this.state.cart
  console.log(cart)
  Object.keys(cart).map(function(key, index){
    console.log(key)
    if (key == idproduct){
      cart[key] -= 1;
    }
  })

  console.log(cart)
  localStorage.setItem("cart",JSON.stringify(cart));

  let total = this.state.total
  let allproduct = this.state.allproduct
  Object.keys(allproduct).map((key, index) => {
    if(idproduct == allproduct[key]['id']){
      total = total - allproduct[key]['price']
    }
  })
  this.setState({
    total: total
  });
}
Xoasp(idproduct){
  let allproduct = this.state.allproduct.filter((item) => item.id != idproduct);
  
  //let cart = JSON.parse(localStorage.getItem('cart')); //get cart form localStorage and convert to array
  //delete allproduct[idproduct.toString()]; //delete item in cart with id delete
  //localStorage.setItem('cart', JSON.stringify(cart)); //convert cart to json and save to localStorage
 // let total = this.state.total - (product.qty * product.price) 
  this.setState({allproduct});

  let cart = this.state.cart
  Object.keys(cart).map(function(key, index){
    if (key == idproduct){
      delete cart[key]
    }
  })
  
  localStorage.setItem("cart",JSON.stringify(cart));
  console.log(Object.keys(cart).length)
	this.context.stateCartContext(Object.keys(cart).length)

//   let allproduct = this.state.allproduct
//  // let total = this.state.total
//   Object.keys(allproduct).map((key, index) => {
//     if (allproduct[key]["id"] == idproduct)
//     {
//      // total = total - (allproduct[key]['price'] * allproduct[key]['qty'])
//       delete allproduct[key]
      
//     }
//   })
//   console.log(allproduct)
//   this.setState({
//     allproduct : allproduct
//    // total: total
//   });


}
fetchData(){
  let allproduct = this.state.allproduct
  console.log(allproduct)
  if (Object.keys(allproduct).length>0){
   
    return Object.keys(allproduct).map((key, index) => {
      console.log(allproduct[key])
      return(
        <Showcart allproduct = {allproduct[key]} Local = {this.Local} Localtru = {this.Localtru} Xoasp = {this.Xoasp}/>

      )
    }
    )
    
  }

}

render() {
  
    return(

    <div className="container">
        <div className="breadcrumbs">
          <ol className="breadcrumb">
            <li><a href="#">Home</a></li>
            <li className="active">Shopping Cart</li>
          </ol>
        </div>
        <div className="table-responsive cart_info">
          <table className="table table-condensed">
            <thead>
              <tr className="cart_menu">
                <td className="image">Item</td>
                <td className="description" />
                <td className="price">Price</td>
                <td className="quantity">Quantity</td>
                <td className="total">Total</td>
                <td />
              </tr>
            </thead>
            <tbody>
              {this.fetchData()}
            </tbody>
          </table>
        </div>
        <div className="container">
        <div className="heading">
          <h3>What would you like to do next?</h3>
          <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <div className="chose_area">
              <ul className="user_option">
                <li>
                  <input type="checkbox" />
                  <label>Use Coupon Code</label>
                </li>
                <li>
                  <input type="checkbox" />
                  <label>Use Gift Voucher</label>
                </li>
                <li>
                  <input type="checkbox" />
                  <label>Estimate Shipping &amp; Taxes</label>
                </li>
              </ul>
              <ul className="user_info">
                <li className="single_field">
                  <label>Country:</label>
                  <select>
                    <option>United States</option>
                    <option>Bangladesh</option>
                    <option>UK</option>
                    <option>India</option>
                    <option>Pakistan</option>
                    <option>Ucrane</option>
                    <option>Canada</option>
                    <option>Dubai</option>
                  </select>
                </li>
                <li className="single_field">
                  <label>Region / State:</label>
                  <select>
                    <option>Select</option>
                    <option>Dhaka</option>
                    <option>London</option>
                    <option>Dillih</option>
                    <option>Lahore</option>
                    <option>Alaska</option>
                    <option>Canada</option>
                    <option>Dubai</option>
                  </select>
                </li>
                <li className="single_field zip-field">
                  <label>Zip Code:</label>
                  <input type="text" />
                </li>
              </ul>
              <a className="btn btn-default update" href>Get Quotes</a>
              <a className="btn btn-default check_out" href>Continue</a>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="total_area">
              <ul>
                <li>Cart Sub Total <span>$59</span></li>
                <li>Eco Tax <span>$2</span></li>
                <li>Shipping Cost <span>Free</span></li>
                <li>Total <span>${this.state.total}</span></li>
              </ul>
              <a className="btn btn-default update" href>Update</a>
              <a className="btn btn-default check_out" href>Check Out</a>
            </div>
          </div>
        </div>
      </div>
      </div>

        );
    }
}
export default Cart;