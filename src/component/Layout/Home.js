import React, { Component } from "react";
import {
	Link,
} from "react-router-dom"
import axios from "axios";
import Producthome from "../Product/Producthome";
import { AppContext } from "./AppContext";

class Home extends Component{
	static contextType = AppContext
	constructor(props) {
        super(props)
		this.state = {
			product:''
		}
		this.fetchData = this.fetchData.bind(this)	
		this.cart = this.cart.bind(this)	
        }
	componentDidMount(){
		axios.get('http://localhost/laravel/laravel/public/api/product')
        .then(res => {
            // console.log(res.data.data)
			this.setState({
				product : res.data.data
			  });
        })
        .catch(error => console.log(error));
	}
	fetchData(){
		let product = this.state.product
		console.log(product)
		if (product instanceof Object){
			return Object.keys(product).map((key, index) => {
				let img = JSON.parse(product[key]["image"])
				return(
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img id="sp1" src={'http://localhost/laravel/laravel/public/upload/user/product/'+product[key]["id_user"]+'/'+img[0]} alt="" />
									<h2>${product[key]["price"]}</h2>
									<p>{product[key]["name"]}</p>
									<Link class="btn btn-default add-to-cart" to={"/product/detail/"+product[key]["id"]}><i class="fa fa-bullseye"></i>Detail</Link>
									<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
								</div>
								<div class="product-overlay">
									<div class="overlay-content">
										<h2>${product[key]["price"]}</h2>
										<p>{product[key]["name"]}</p>
										<Link class="btn btn-default add-to-cart" to={"/product/detail/"+product[key]["id"]} ><i class="fa fa-bullseye"></i>Detail</Link>
										<a class="btn btn-default add-to-cart" id={product[key]["id"]} onClick={this.cart} ><i class="fa fa-shopping-cart"></i>Add to cart</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				)
			  })
		}

	}
	cart(e){
		// add vao local roi dem ra so sanh
		let id = e.target.id
		
		let cart = {}
		// cart[id] = 1
		// let cartmenu =0
		let x=1
		var getLocal=localStorage.getItem("cart")
		if(getLocal) {
			cart=JSON.parse(getLocal);
			Object.keys(cart).map(function(key, index){
				if (key == id){
					cart[key] += 1;
					// console.log(cart[key])
					x=2
				}

			})
		}
		if(x==1){
			cart[id] = 1
		}

		localStorage.setItem("cart",JSON.stringify(cart));
		// console.log(Object.keys(cart).length)
		this.context.stateCartContext(Object.keys(cart).length)


	}
    render() {
        return(
            <div>
            <section id="slider">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>Free E-Commerce Template</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="images/home/girl1.jpg" class="girl img-responsive" alt="" />
									<img src="images/home/pricing.png"  class="pricing" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>100% Responsive Design</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="images/home/girl2.jpg" class="girl img-responsive" alt="" />
									<img src="images/home/pricing.png"  class="pricing" alt="" />
								</div>
							</div>
							
							<div class="item">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>Free Ecommerce Template</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="images/home/girl3.jpg" class="girl img-responsive" alt="" />
									<img src="images/home/pricing.png" class="pricing" alt="" />
								</div>
							</div>
							
						</div>
						
						<Link to="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</Link>
						<Link to="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</Link>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<Link data-toggle="collapse" data-parent="#accordian" to="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Sportswear
										</Link>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><Link to="#">Nike </Link></li>
											<li><Link to="#">Under Armour </Link></li>
											<li><Link to="#">Adidas </Link></li>
											<li><Link to="#">Puma</Link></li>
											<li><Link to="#">ASICS </Link></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<Link data-toggle="collapse" data-parent="#accordian" to="#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Mens
										</Link>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><Link to="#">Fendi</Link></li>
											<li><Link to="#">Guess</Link></li>
											<li><Link to="#">Valentino</Link></li>
											<li><Link to="#">Dior</Link></li>
											<li><Link to="#">Versace</Link></li>
											<li><Link to="#">Armani</Link></li>
											<li><Link to="#">Prada</Link></li>
											<li><Link to="#">Dolce and Gabbana</Link></li>
											<li><Link to="#">Chanel</Link></li>
											<li><Link to="#">Gucci</Link></li>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<Link data-toggle="collapse" data-parent="#accordian" to="#womens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Womens
										</Link>
									</h4>
								</div>
								<div id="womens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><Link to="#">Fendi</Link></li>
											<li><Link to="#">Guess</Link></li>
											<li><Link to="#">Valentino</Link></li>
											<li><Link to="#">Dior</Link></li>
											<li><Link to="#">Versace</Link></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><Link to="#">Kids</Link></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><Link to="#">Fashion</Link></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><Link to="#">Households</Link></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><Link to="#">Interiors</Link></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><Link to="#">Clothing</Link></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><Link to="#">Bags</Link></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><Link to="#">Shoes</Link></h4>
								</div>
							</div>
						</div>
					
						<div class="brands_products">
							<h2>Brands</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li><Link to="#"> <span class="pull-right">(50)</span>Acne</Link></li>
									<li><Link to="#"> <span class="pull-right">(56)</span>Grüne Erde</Link></li>
									<li><Link to="#"> <span class="pull-right">(27)</span>Albiro</Link></li>
									<li><Link to="#"> <span class="pull-right">(32)</span>Ronhill</Link></li>
									<li><Link to="#"> <span class="pull-right">(5)</span>Oddmolly</Link></li>
									<li><Link to="#"> <span class="pull-right">(9)</span>Boudestijn</Link></li>
									<li><Link to="#"> <span class="pull-right">(4)</span>Rösch creative culture</Link></li>
								</ul>
							</div>
						</div>
						
						<div class="price-range">
							<h2>Price Range</h2>
							<div class="well text-center">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" /><br />
								 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
							</div>
						</div>
						
						<div class="shipping text-center">
							<img src="images/home/shipping.jpg" alt="" />
						</div>
					
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items">
						<h2 class="title text-center">Features Items</h2>
						{this.fetchData()}		
						{/* <Producthome /> */}
					</div>
					
					<div class="category-tab">
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><Link to="#tshirt" data-toggle="tab">T-Shirt</Link></li>
								<li><Link to="#blazers" data-toggle="tab">Blazers</Link></li>
								<li><Link to="#sunglass" data-toggle="tab">Sunglass</Link></li>
								<li><Link to="#kids" data-toggle="tab">Kids</Link></li>
								<li><Link to="#poloshirt" data-toggle="tab">Polo shirt</Link></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="tshirt" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery4.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="tab-pane fade" id="blazers" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery4.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="tab-pane fade" id="sunglass" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery4.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="tab-pane fade" id="kids" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery4.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="tab-pane fade" id="poloshirt" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery4.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/gallery1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="recommended_items">
						<h2 class="title text-center">recommended items</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend1.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend2.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend3.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="item">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend1.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend2.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="images/home/recommend3.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							 <Link class="left recommended-item-control" to="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </Link>
							  <Link class="right recommended-item-control" to="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </Link>			
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
    </div>
        );
    }
}
export default Home;