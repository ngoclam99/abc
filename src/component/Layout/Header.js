import React, { Component } from "react";
import {
	Link,
  } from "react-router-dom"
import { withRouter } from 'react-router';
import { AppContext } from "./AppContext";
class Header extends Component {
	static contextType = AppContext
	constructor(props){
		super(props)

		this.checkLogin = this.checkLogin.bind(this);
		this.logout = this.logout.bind(this);
		this.checkcart = this.checkcart.bind(this)
	
	}
	checkLogin(){
		var getlocal = localStorage.getItem("islogin")
		console.log(getlocal)
    		if(getlocal) {
				
        		var index=JSON.parse(getlocal);
				if (index == true){
					return(
						<li><a onClick={this.logout}><i className="fa fa-shopping-cart" />Logout</a></li>
					)
				}
				
			}	
			else {
				return(
				
						<li><Link to="/login"><i className="fa fa-shopping-cart" />Login</Link></li>
					
				)
			}
	}	
	logout(){
		localStorage.clear();
		this.props.history.push('/login')
	}
	checkcart(){
		var getlocal = localStorage.getItem("cartmenu")
		console.log(getlocal)
    		if(getlocal) {
        		var index=JSON.parse(getlocal);
				if (index != 0){
					return(
						<li><Link to="/product/cart"><i className="fa fa-shopping-cart" /> Cart ({index})</Link></li>
					)
				}
			}	
			else {
				return(
					<li><Link to="/product/cart"><i className="fa fa-shopping-cart" /> Cart</Link></li>
				)
			}
	}
    render() {
      return (
		  
		<header id="header">{/*header*/}
			<div className="header_top">{/*header_top*/}
			<div className="container">
				<div className="row">
				<div className="col-sm-6">
					<div className="contactinfo">
					<ul className="nav nav-pills">
						<li><Link to="#"><i className="fa fa-phone" /> +2 95 01 88 821</Link></li>
						<li><Link to="#"><i className="fa fa-envelope" /> info@domain.com</Link></li>
					</ul>
					</div>
				</div>
				<div className="col-sm-6">
					<div className="social-icons pull-right">
					<ul className="nav navbar-nav">
						<li><Link to="#"><i className="fa fa-facebook" /></Link></li>
						<li><Link to="#"><i className="fa fa-twitter" /></Link></li>
						<li><Link to="#"><i className="fa fa-linkedin" /></Link></li>
						<li><Link to="#"><i className="fa fa-dribbble" /></Link></li>
						<li><Link to="#"><i className="fa fa-google-plus" /></Link></li>
					</ul>
					</div>
				</div>
				</div>
			</div>
			</div>{/*/header_top*/}
			<div className="header-middle">{/*header-middle*/}
			<div className="container">
				<div className="row">
				<div className="col-md-4 clearfix">
					<div className="logo pull-left">
					<Link to="index.html"><img src="images/home/logo.png" alt="" /></Link>
					</div>
					<div className="btn-group pull-right clearfix">
					<div className="btn-group">
						<button type="button" className="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
						USA
						<span className="caret" />
						</button>
						<ul className="dropdown-menu">
						<li><Link >Canada</Link></li>
						<li><Link >UK</Link></li>
						</ul>
					</div>
					<div className="btn-group">
						<button type="button" className="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
						DOLLAR
						<span className="caret" />
						</button>
						<ul className="dropdown-menu">
						<li><Link >Canadian Dollar</Link></li>
						<li><Link >Pound</Link></li>
						</ul>
					</div>
					</div>
				</div>
				<div className="col-md-8 clearfix">
					<div className="shop-menu clearfix pull-right">
						<ul className="nav navbar-nav">
							<li><Link to="/account/member"><i className="fa fa-user" /> Account</Link></li>
							<li><a><i className="fa fa-star" /> Wishlist</a></li>
							<li><a to="checkout.html"><i className="fa fa-crosshairs" /> Checkout</a></li>
							{this.checkcart()}
							{this.checkLogin()}
							
						</ul>
						
					
					</div>
				</div>
				</div>
			</div>
			</div>{/*/header-middle*/}
			<div className="header-bottom">{/*header-bottom*/}
			<div className="container">
				<div className="row">
				<div className="col-sm-9">
					<div className="navbar-header">
					<button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span className="sr-only">Toggle navigation</span>
						<span className="icon-bar" />
						<span className="icon-bar" />
						<span className="icon-bar" />
					</button>
					</div>
					<div className="mainmenu pull-left">
					<ul className="nav navbar-nav collapse navbar-collapse">
						<li><Link to="/" className="active">Home</Link></li>
						<li className="dropdown"><Link to="#">Shop<i className="fa fa-angle-down" /></Link>
						<ul role="menu" className="sub-menu">
							<li><Link to="shop.html">Products</Link></li>
							<li><Link to="product-details.html">Product Details</Link></li> 
							<li><Link to="checkout.html">Checkout</Link></li> 
							<li><Link to="">Cart</Link></li> 
						</ul>
						</li> 
						<li className="dropdown"><Link to="#">Blog<i className="fa fa-angle-down" /></Link>
						<ul role="menu" className="sub-menu">
							<li><Link to="/blog/list">Blog List</Link></li>
							<li><Link to="">Blog Single</Link></li>
						</ul>
						</li> 
						<li><Link to="404.html">404</Link></li>
						<li><Link to="contact-us.html">Contact</Link></li>
					</ul>
					</div>
				</div>
				<div className="col-sm-3">
					<div className="search_box pull-right">
					<input type="text" placeholder="Search" />
					</div>
				</div>
				</div>
			</div>
			</div>{/*/header-bottom*/}
		</header>
			  );
			}
	
  }
    export default withRouter(Header); 
