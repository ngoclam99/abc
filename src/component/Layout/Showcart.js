import React, { Component } from "react";
import axios from "axios";


class Showcart extends Component{
	constructor(props) {
    super(props)
		this.state = {
            allproduct :this.props.allproduct,
            qty: 1,
            tong: 0,
            newQty:1

            
        }
    this.fetchData = this.fetchData.bind(this)		
    this.cong = this.cong.bind(this)
    this.tru = this.tru.bind(this)
    this.xoa = this.xoa.bind(this)
	}
componentDidMount(){
  let {allproduct} = this.props
  this.setState({
   qty: allproduct['qty'],
   tong: allproduct['qty'] * allproduct['price'],
   price: allproduct['price']
 })
}

cong(e){
    let qty = this.state.qty
    // qty = qty + 1
    // let tong = this.state.tong
    let price= this.state.price
    let tong =(qty+1) * price
    this.setState({
        qty : qty+1,
        tong : tong
      });
//truyen id qua cart de so sanh trong local
    let id = e.target.id
    // console.log(id)
    this.props.Local(id)
}
tru(e){
  // console.log(this.state.qty)
  let qty = this.state.qty
  // qty = qty + 1
  let tong = this.state.tong
  let price= this.state.price
  tong =(qty-1) * price
  this.setState({
      qty : qty-1,
      tong : tong
    });
  //Local
  let id = e.target.id
  // console.log(id)
  this.props.Localtru(id)
}
xoa(e){
  let id = e.target.id
  this.props.Xoasp(id)

}
fetchData(){
  let allproduct = this.props.allproduct
  let qtystate = this.state.qty
  let tongstate = this.state.tong
  let tongsp = allproduct.qty *  allproduct.price
  let qty = qtystate 
  let tong = tongstate ? tongstate : tongsp
  console.log(tongstate)
  // if(tongstate!=0){
  //    tong = tongstate
  // }
  // else{
  //    tong = allproduct.qty *  allproduct.price
  // }
  // console.log(tongstate)
  // console.log(allproduct)
  let img = JSON.parse(allproduct["image"])
  return(
        <tr>
          <td className="cart_product">
            <a href><img src={'http://localhost/laravel/laravel/public/upload/user/product/'+allproduct["id_user"]+'/small_'+img[0]} alt="" /></a>
          </td>
          <td className="cart_description">
            <h4><a href>{allproduct["name"]}</a></h4>
            <p>ID: {allproduct["id"]}</p>
          </td>
          <td className="cart_price">
            <p>${allproduct["price"]}</p>
          </td>
          <td className="cart_quantity">
            <div className="cart_quantity_button">
                <a className="cart_quantity_up" id = {allproduct["id"]} onClick={this.cong}> + </a>
                <input className="cart_quantity_input" type="text" name="quantity" value={qty} autoComplete="off" size={2} />
                <a className="cart_quantity_down" id= {allproduct["id"]} onClick={this.tru} > - </a>
              </div>
          </td>
          <td className="cart_total">
            <p className="cart_total_price">${tong}</p>
          </td>
          <td className="cart_delete">
            <a className="cart_quantity_delete" id= {allproduct["id"]} onClick={this.xoa}>xoa</a>
          </td>
        </tr>
      )
}

render() {
   
  
    return(
        <>
            {this.fetchData()}
        </>

        );
    }
}
export default Showcart;