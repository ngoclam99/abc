import React, { Component } from "react";
import Err from "../Account/Member/Err";
import axios from "axios";
import {
	Link
  } from "react-router-dom"
import { AppContext } from "./AppContext";
class Login extends Component {
  static contextType = AppContext
  constructor(props){
    super(props)
    this.state = {
      name : '',
      email : '',
      pass : '',
      phone :'',
      add : '',
      formErr: {},
      file : '',

      emaillg : '',
      passlg : '',
      level: '0',
    }
    this.showname = this.showname.bind(this)
    this.showemail = this.showemail.bind(this)
    this.showpass = this.showpass.bind(this)
    this.showphone = this.showphone.bind(this)
    this.showadd = this.showadd.bind(this)
    this.xulyfile = this.xulyfile.bind(this)
    this.submitFormSingup = this.submitFormSingup.bind(this)
    this.showemaillg = this.showemaillg.bind(this)
    this.showpasslg = this.showpasslg.bind(this)
    this.submitFormLogin = this.submitFormLogin.bind(this)
  }
  //Register
  showname(e){
    this.setState({
      name: e.target.value
    })
  }
  showemail(e){
    this.setState({
      email: e.target.value
    })
  }
  showpass(e){
    this.setState({
      pass: e.target.value
    })
  }
  showphone(e){
    this.setState({
      phone: e.target.value
    })
  }
  showadd(e){
    this.setState({
      add: e.target.value
    })
  }
  xulyfile(e){
    let file = e.target.files
    let reader = new FileReader();
      reader.onload = (e) => {
        this.setState({
          avatar : e.target.result,
          file : file[0]
        })
      }
      reader.readAsDataURL(file[0])
  }

  submitFormSingup(e){
    e.preventDefault(); 
    let name = this.state.name
    let email = this.state.email
    let pass = this.state.pass
    let phone = this.state.phone
    let add = this.state.add
    let file = this.state.file
    let avatar = this.state.avatar
    let err =this.state.formErr;
    let flag=true
    err.name = ""
    err.email= ""
    err.pass = ""
    err.file =""
    err.phone = ""
    err.add = ""
    const kt = /\S+@\S+\.\S+/;
    let object={}
    let img =['png', 'jpg', 'jpeg', 'PNG', 'JPG']
    const dt = /^\d{10,11}$/

    

    if (!name) {
      flag=false;
      err.name="Nhap ten";
    }

    if (email=="") {
      flag=false;
      err.email="Nhap email";
    }
    else{
    if (!kt.test(String(email))) {
      flag=false;
      err.email= "Sai dinh dang email"
    }


    }
    if (!pass) {
      flag=false;
      err.pass="Nhap pass";
    }

    if (!add) {
      flag=false;
      err.add="Nhap dia chi";
    }

    if (!phone) {
      flag=false;
      err.phone="Nhap sdt";
    }
    else{
      if(!dt.test(phone)){
        flag= false;
        err.phone ="so dien thoai co 10 so"
      }

    }
    if (!file) {
      flag=false;
      err.file="chon file";
    }
    else{
      if ( file['size'] > 1024 * 1024 ) {
        flag=false;
        err.file = "dung luong qua lon"
    
      }
      else {
        let abc = file['name'].split(".");
        // console.log(abc)
        if(!img.includes(abc[1])) {
          err.file = "khong dung dinh dang hinh anh"
          flag=false;
        }
      }
    }
    if(!flag){
      this.setState({
        formErr : err
      });
    }
    else{
      this.setState({
        formErr : {}
      });
      let obj={
        'name' : name,
        'email': email,
        'password' : pass,
        'phone' : phone,
        'address': add,
        'avatar' :avatar,
        'level':0

      }
      axios.post(`http://localhost/laravel/laravel/public/api/register`, obj)
        .then(res => {
          console.log(res.data)
            if(res.data.success){
              console.log("ok")
            }
            else{
              console.log(res.data.errors)
              // this.setState({
              //   // formErr : res.data.errors
              // });
            }
        })
        .catch(error => console.log(error));
    }
  }
//Login
showemaillg(e){
    this.setState({
    emaillg: e.target.value
    })
 }
showpasslg(e){
    this.setState({
    passlg: e.target.value
    })
}
submitFormLogin(e){
    e.preventDefault();
    let emaillg = this.state.emaillg
    let passlg = this.state.passlg
    let err =this.state.formErr;
    let level = "0"
    err.emaillg= ""
    err.passlg = "" 
    let flag= true
    const kt = /\S+@\S+\.\S+/;

    if (emaillg =="") {
        flag=false;
        err.emaillg="Nhap email";
      }
    else{
      if (!kt.test(String(emaillg))) {
        flag=false;
        err.emaillg= "Sai dinh dang email"
      }
    }

    if (!passlg) {
        flag=false;
        err.passlg="Nhap pass";
    }

    if(!flag){
        this.setState({
          formErr : err
        });
      }
    else{
        this.setState({
          formErr : {}
        });
        let obj={
            'email': emaillg,
            'password' : passlg
    
        }
        axios.post(`http://localhost/laravel/laravel/public/api/login`, obj)
            .then(res => {
              console.log(res.data)
                if(res.data.success){
                
                  this.context.loginContext(true)

                  let islogin=true
                  let ob= res.data
                  let object ={
                    islogin,
                    ob
                  }
                  localStorage.setItem("a", JSON.stringify(object))
                  this.props.history.push('/')
                }
                else{
                  console.log(res.data.errors)
                  this.setState({
                    formErr : res.data.errors
                  });
                }
            })
            .catch(error => console.log(error));
    }
  }
    render() {
      return (
        <section id="form">{/*form*/}
        <div className="container">
          <div className="row">
            <div className="col-sm-4 col-sm-offset-1">
              <div className="login-form">{/*login form*/}
                <h2>Login to your account</h2>
                <form onSubmit={this.submitFormLogin}>
                  <input type="email" placeholder="Email" value={this.state.emaillg} onChange={this.showemaillg} />
                  <input type="password" placeholder="Password" value={this.state.passlg} onChange={this.showpasslg} />
                  <span>
                    <input type="checkbox" className="checkbox" /> 
                    Keep me signed in
                  </span>
                  <Err formErr={this.state.formErr} />
                  <button type="submit" className="btn btn-default">Login</button>
                </form>
              </div>{/*/login form*/}
            </div>
            <div className="col-sm-1">
              <h2 className="or">OR</h2>
            </div>
            <div className="col-sm-4">
              <div className="signup-form">{/*sign up form*/}
                <h2>New User Signup!</h2>
                <form encType="multipart/form-data" onSubmit={this.submitFormSingup}>
                  
                  <input type="text" placeholder="Name" value={this.state.name} onChange={this.showname} />
                  <input type="email" placeholder="Email Address" value={this.state.email} onChange={this.showemail} />
                  <input type="password" placeholder="Password" value={this.state.pass} onChange={this.showpass} />
                  <input type="text" placeholder="Phone" value={this.state.phone} onChange={this.showphone} />
                  <input type="text" placeholder="Address" value={this.state.add} onChange={this.showadd} />
                  <input type="" placeholder="Level" value="0"/>
                  <input type="file" onChange={this.xulyfile}></input>
                  <Err formErr={this.state.formErr} />
                  <button type="submit" className="btn btn-default">Signup</button>
                </form>
              </div>{/*/sign up form*/}
            </div>
          </div>
        </div>
      </section>
      );
    }
}
export default Login; 