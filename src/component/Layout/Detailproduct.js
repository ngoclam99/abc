import React, { Component } from "react";
import {
	Link,
} from "react-router-dom"
import axios from "axios";


class Detailproduct extends Component{
	constructor(props) {
        super(props)
		this.state = {
			detailproduct: '',
			listCategory:'',
			listBrand:'',
			img: [], 
			larger:''

        }
		this.fetchData = this.fetchData.bind(this)
		this.hienthi = this.hienthi.bind(this)
	}
	componentDidMount(){
		axios.get('http://localhost/laravel/laravel/public/api/product/detail/'+this.props.match.params.id)
        .then(res => {
            // console.log(res.data.data)
			let img = JSON.parse( res.data.data["image"])
			// console.log(img)
			this.setState({
				detailproduct : res.data.data,
				img : img,
				larger : img[0]
			  });
        })
        .catch(error => console.log(error));
		axios.get('http://localhost/laravel/laravel/public/api/category-brand')
        .then(res => {
            // console.log(res.data)
            this.setState({
                listCategory: res.data.category,
                listBrand: res.data.brand
            });
        })
        .catch(error => console.log(error));
	}
	fetchData(){
		let detailproduct = this.state.detailproduct
		// console.log(detailproduct)
		// console.log(detailproduct["image"])
		// let img = JSON.parse(detailproduct["image"])

		let img = this.state.img
		// console.log(img)
		let listBrand = this.state.listBrand
		let listCategory = this.state.listCategory
		let brand = ''
		let category =''
		let larger = this.state.larger
		// console.log(larger)
		if (listBrand instanceof Array){
			listBrand.map((value, index) => {
				if (detailproduct["id_brand"] == value.id){
					brand = value.brand
				}
			})
		  }
		if (listCategory instanceof Array){
			listCategory.map((value, index) => {
				if (detailproduct["id_category"] == value.id){
					category = value.category
				}
			})
		  }

		return(
			<div className="product-details">
				<div className="col-sm-5">
					<div className="view-product">
						<img name="larger" src={'http://localhost/laravel/laravel/public/upload/user/product/'+detailproduct["id_user"]+'/larger_'+larger} alt="" />
						<a href={'http://localhost/laravel/laravel/public/upload/user/product/'+detailproduct["id_user"]+'/'+larger} rel="prettyPhoto"><h3>ZOOM</h3></a>
					</div>
					<div id="similar-product" className="carousel slide" data-ride="carousel">
						{/* Wrapper for slides */}
						<div class="carousel-inner">
							<div class="item active">
								<a href=""><img src={'http://localhost/laravel/laravel/public/upload/user/product/'+detailproduct["id_user"]+'/small_'+img[1]} alt="" onClick={this.hienthi}/></a>
								<a href=""><img src={'http://localhost/laravel/laravel/public/upload/user/product/'+detailproduct["id_user"]+'/small_'+img[0]} alt="" onClick={this.hienthi} /></a>
							</div>
							<div class="item">
								<a href=""><img src={'http://localhost/laravel/laravel/public/upload/user/product/'+detailproduct["id_user"]+'/small_'+img[1]} alt="" /></a>
								<a href=""><img src={'http://localhost/laravel/laravel/public/upload/user/product/'+detailproduct["id_user"]+'/small_'+img[0]} alt="" /></a>
							</div>
							{/* <div class="item">
								<a href=""><img src={'http://localhost/laravel/laravel/public/upload/user/product/'+detailproduct["id_user"]+'/small_'+img[1]} alt="" onClick={this.hienthi}/></a>
								<a href=""><img src={'http://localhost/laravel/laravel/public/upload/user/product/'+detailproduct["id_user"]+'/small_'+img[0]} alt="" /></a>
							</div> */}
										
						</div>
							{/* Controls */}
							<a className="left item-control" data-slide="prev">
								<i className="fa fa-angle-left" />
							</a>
							<a className="right item-control" data-slide="next">
								<i className="fa fa-angle-right" />
							</a>
					</div>
				</div>
				<div className="col-sm-7">
					<div className="product-information">{/*/product-information*/}
						<img src="images/product-details/new.jpg" className="newarrival" alt="" />
						<h2>{detailproduct["name"]}</h2>
						<img src="images/product-details/rating.png" alt="" />
						<span>
							<span>US ${detailproduct["price"]}</span>
							<label>Quantity:</label>
							<input type="text" defaultValue={1} />
							<button type="button" className="btn btn-fefault cart">
								<i className="fa fa-shopping-cart" /><Link to="/product/cart">Add to cart</Link>
							</button>
						</span>
						<p><b>Category:</b> {category}</p>
						<p><b>Brand:</b> {brand}</p>
						{/* <a href><img src="/images/product-details/share.png" className="share img-responsive" alt="" /></a> */}
					</div>{/*/product-information*/}
				</div>
			</div>
		)
		

	}
	hienthi(e){
		e.preventDefault();
		const value = e.target.src
		console.log(value)
		let x=value.split("small_")
		console.log(x[1])
		this.setState({
			larger : x[1]
		})
	}
    render() {
        return(
			<div className="container detailproduct">
				{this.fetchData()}
			</div>

        );
    }
}
export default Detailproduct;

// {
// 	18: 2,
// 	20: 1
// }