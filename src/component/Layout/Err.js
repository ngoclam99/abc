import React, { Component } from "react";
const Style={
    color: 'red'
  };
class Err extends Component {
    renderError(){
        let formErr=this.props.formErr
        if(Object.keys(formErr).length>0){
          return Object.keys(formErr).map((key,i) => {
            return (
              <p style={Style} key={i}> {formErr[key]} </p>
            )
          })
        }
      }
      render(){
        return(
            <div>
              {this.renderError()}
            </div>
        )
      }  
}
    export default Err