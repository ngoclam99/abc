import React, { Component } from "react";
import {
	Link,
} from "react-router-dom"
import axios from "axios";
import { withRouter } from 'react-router';
class Producthome extends Component{
	constructor(props) {
        super(props)
        this.state = {
			product:'',
		}
		this.componentDidMount = this.componentDidMount.bind(this);
		this.fetchData = this.fetchData.bind(this)
        }
        componentDidMount(){
            axios.get('http://localhost/laravel/laravel/public/api/product')
            .then(res => {
                // console.log(res.data.data)
                this.setState({
                    product : res.data.data
                  });
            })
            .catch(error => console.log(error));
        }
        fetchData(){
            let product = this.state.product
            // console.log(product)
            if (product instanceof Object){
                return Object.keys(product).map((key, index) => {
                    // console.log(product[key]["name"])
                    return(
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <img id="sp1" src="images/home/product1.jpg" alt="" />
                                        <h2>$56</h2>
                                        <p>{product[key]["name"]}</p>
                                        <Link class="btn btn-default add-to-cart" to={'/product-detail/'+product[key]["id"]}><i class="fa fa-bullseye"></i>Detail</Link>
                                        <Link to="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
                                    </div>
                                    <div class="product-overlay">
                                        <div class="overlay-content">
                                            <h2>$56</h2>
                                            <p>{product[key]["name"]}</p>
                                            <Link class="btn btn-default add-to-cart" to={'/product-detail/'+product[key]["id"]} ><i class="fa fa-bullseye"></i>Detail</Link>
                                            <Link class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    )
                  })
            }
    
        }
    render() {
        return(
			this.fetchData()	
        );
    }
}
export default withRouter(Producthome);