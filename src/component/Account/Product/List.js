import React, { Component } from 'react'
import { Link } from "react-router-dom";
import axios from "axios";
import Deleteproduct from './Delete';
class List extends Component {
    constructor(props) {
        super(props)
		this.state = {
			product:'',
			account : JSON.parse(localStorage["a"])
		}
		this.componentDidMount = this.componentDidMount.bind(this);
		this.fetchData = this.fetchData.bind(this)
        // this.removeproduct = this.removeproduct.bind(this)
		
        }
	componentDidMount(){
		let account = this.state.account
		// console.log(account)
  		let accessToken = account.ob['success']['token'] 
  		let config = {                
			headers: {                
			'Authorization': 'Bearer '+ accessToken,                
			'Content-Type': 'application/x-www-form-urlencoded',                
			'Accept': 'application/json'                
			}                
			};
		// console.log(accessToken)
		axios.get('http://localhost/laravel/laravel/public/api/user/my-product', config)
        .then(res => {
            // console.log(res.data.data)
			this.setState({
				product : res.data.data
			  });
        })
        .catch(error => console.log(error));
	}
    removeproduct(product){
        this.setState({
            product: product
        });
    }
	fetchData(){
		let product = this.state.product
        // console.log(product)
        let account = this.state.account
		if (product instanceof Object){
			return Object.keys(product).map((key, index) => {
				// console.log(product[key]["image"])
                let image = JSON.parse(product[key]["image"])
                // console.log(image)
				return(
                    <tr key={index}>
                        <td className="cart_quantity">
                            <a>{product[key]["id"]}</a>
                        </td>
                        <td className="cart_description">
                            <a>{product[key]["name"]}</a>
                        </td>
                        
                        <td >
                            <img width="80" height="70" src={"http://localhost/laravel/laravel/public/upload/user/product/" + account.ob["Auth"]["id"] + "/" + image[0]} />
						</td>
                        <td className="cart_price">
                            <p>${product[key]["price"]}</p>
                        </td>   
                        <td>
                            <Link to={"/account/product/edit-product/"+product[key]["id"]} className="cart_quantity_edit"><i className="fa fa-edit" /></Link>
                        </td>
                        <td>
                            {/* <Delete id = {product[key]["id"]} /> */}
                            <Deleteproduct id={product[key]["id"]} removeproduct={this.removeproduct.bind(this)} />
                        </td>
                    </tr>
					
				)
			  })
		}
    
	}
  	render () {
	   return (
        <div className="col-sm-9">
        <div className="col-sm-12" id="cart_items">
            <div className="table-responsive cart_info">
                <table className="table table-condensed">
					<thead>
                        <tr className="cart_menu">
                            <td>Id</td>
                            <td className="price">Name</td>
                            <td >Image</td>
                            <td className="description">Price</td>
                            <td className="total" colSpan="2">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.fetchData()}
                    </tbody>
                </table>
                <Link to="/account/product/add" className="btn btn-default check_out">Add New</Link>
            </div>
        </div>
        </div>
    )
  }
}
export default List
