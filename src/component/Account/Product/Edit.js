import React, { Component } from 'react'
import { Link } from "react-router-dom";
import axios from "axios";
import Err from '../Member/Err';
const inputSale = {
    width: '30%',
    display:'block'
};
const img = {
    display: 'inline-block',
    margin: '0 10px'
};
class Edit extends Component {
    constructor(props) {
        super(props)
		this.state = {
			listCategory:'',
            listBrand:'',
            category: '',
            brand: '',
            name: '',
            price: '',
            company:'',
            file:'',
            listfile: [],
            detail:'',
            user: JSON.parse(localStorage["a"]),
            status: 0,
            sale:0,
            formErr: {},
            avatarCheckBox:[],
		}
        this.componentDidMount = this.componentDidMount.bind(this)
        this.showcategory = this.showcategory.bind(this)
        this.showbrand = this.showbrand.bind(this)
        this.showvalue = this.showvalue.bind(this)
        this.xulyfile = this.xulyfile.bind(this)
        this.formeditproduct = this.formeditproduct.bind(this)
        this.showimage = this.showimage.bind(this)
        this.checkbox = this.checkbox.bind(this)


		
        }
    componentDidMount(){
        let user = this.state.user
        let accessToken = user.ob['success']['token']  
            let config = {                
                headers: {                
                'Authorization': 'Bearer '+ accessToken,                
                'Content-Type': 'application/x-www-form-urlencoded',                
                'Accept': 'application/json'                
                }                
            }; 
        let id = this.props.match.params.id
        let url='http://localhost/laravel/laravel/public/api/user/product/'+id
        axios.get(url, config)
        .then(res => {
            // console.log(res.data.data.image)
			this.setState({
				name: res.data.data.name,
                price: res.data.data.price,
                category: res.data.data.id_category,
                brand: res.data.data.id_brand,
                company: res.data.data.company_profile,
                detail: res.data.data.detail,
                status: res.data.data.status,
                sale: res.data.data.sale,
                fileold: res.data.data.image,
			  });
        })
        .catch(error => console.log(error));

        axios.get('http://localhost/laravel/laravel/public/api/category-brand')
        .then(res => {
            // console.log(res.data)
            this.setState({
                listCategory: res.data.category,
                listBrand: res.data.brand
            });
        })
        .catch(error => console.log(error));
    }
    showcategory() {
        let listCategory = this.state.listCategory
        if (listCategory instanceof Array){
            return listCategory.map((value, index) => {
                return (
                    <option key={index} index={index} value={value.id}>
                        {value.category}
                    </option>
                )
            })
        }  
    }
    showbrand() {
        let listBrand = this.state.listBrand
        if (listBrand instanceof Array){
            return listBrand.map((value, index) => {
            //   console.log(value.id)
            return (
                <option key={index} index={index} value={value.id}>
                    {value.brand}
                </option>
            )
            })
        }  
    }
    showsale() {
        let status = this.state.status
        if(status == 2) {
            return (
                <div className="col-sm-12">
                    <input className="col-sm-2" style={inputSale} type="text" name="sale" value={this.state.sale} onChange={this.showvalue} />
                    <p className="col-sm-2">%</p>
                </div>
            )
        }
    }
    showvalue(e){
        const nameinput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameinput]: value
        })
    }

    xulyfile(e){
        let file = e.target.files
        this.setState({
            file : file
        })
    }
    showimage() {
        let fileold = this.state.fileold
        let user = this.state.user

        if (Array.isArray(fileold) && fileold.length > 0) {
            return fileold.map((value, index) => {
                return (
                    <li style={img} key={index}>
                        <img width="90" height="90" src={'http://localhost/laravel/laravel/public/upload/user/product/' + user.ob.Auth.id + '/' + value} alt="" />
                        <input name="checkimg" type="checkbox" value={value} onChange={this.showvalue} onClick={this.checkbox} />
                    </li>
                )
            }) 
        }
    }

    checkbox(e){
        const isCheck = e.target.checked
        const value = e.target.value;
        let avatarCheckBox = this.state.avatarCheckBox
        console.log(value)
        if(isCheck) {
            avatarCheckBox.push(value)
            this.setState({
                avatarCheckBox : avatarCheckBox
            })
        }
        else{
            // const new_arr = filecheck.filter(item => item !== value)
            let vitri = avatarCheckBox.indexOf(value); //indexOff: tìm vị trí của value trong mang
            avatarCheckBox.splice(vitri, 1)
            this.setState({
                avatarCheckBox : avatarCheckBox
            })
        }
        console.log(avatarCheckBox)
    }

    formeditproduct(e){
        e.preventDefault();
        let name = this.state.name;
        let price = this.state.price;
        let category = this.state.category;
        let brand = this.state.brand;
        let status = this.state.status;
        let sale = this.state.sale;
        let company = this.state.company;
        let detail = this.state.detail;
        let file = this.state.file
        let user = this.state.user
        let err =this.state.formErr;
        let avatarCheckBox = this.state.avatarCheckBox
        let fileold = this.state.fileold
        let flag=true
        err.name = err.price = err.category = err.brand = err.status = err.sale = err.company = err.detail = err.file = ""
        let img =['png', 'jpg', 'jpeg', 'PNG', 'JPG']

        if (!name) {
            flag=false;
            err.name="Nhap ten";
          }
        if (!price) {
            flag=false;
            err.price="Nhap price";
          }
        else {
            if(isNaN(price)){
                flag=false;
                err.price="price la so";
            }
        }
        if (!category) {
            flag=false;
            err.category="chon catology";
          }
        if (!brand) {
            flag=false;
            err.brand="chon brand";
          }
        if (!status) {
            flag=false;
            err.status="chon status";
          }
        if(!sale && status==2){
            flag=false;
            err.sale="nhap sale";
        }
        if (!company) {
            flag=false;
            err.company="nhap company";
          }
        if (!detail) {
            flag=false;
            err.detail="nhap detail";
          }
        if (file) {
            if(file instanceof Object){
                Object.keys(file).map((key, index) => {
                    // console.log(file[key].size)
                    if (file[key].size > 1024 * 1024 ) {
                        flag=false;
                        err.file = "dung luong qua lon"
                    }
                    else {
                        let dinhdang = file[key].name.split(".");
                        if(!img.includes(dinhdang[1])) {
                        err.file = "khong dung dinh dang hinh anh"
                        flag=false;
                        }
                       
                    }
                })
            }
        }
        let a = file.length + (fileold.length - avatarCheckBox.length)
        console.log('tong hinh con lai:'+ a)
        if(a > 3){
            flag=false;
            err.file="chon toi da 3 hinh";
        }

        if(!flag){
            this.setState({
              formErr : err
            });
          }
          else{
            this.setState({
              formErr : {}
            });
            // console.log(name, price, category, brand, company, detail, status, sale)
            
            let accessToken = user.ob['success']['token']  
            let config = {                
                headers: {                
                'Authorization': 'Bearer '+ accessToken,                
                'Content-Type': 'application/x-www-form-urlencoded',                
                'Accept': 'application/json'                
                }                
            }; 
            let url='http://localhost/laravel/laravel/public/api/user/edit-product/' + this.props.match.params.id      
            const formData = new FormData();                
            formData.append('name', name);
            formData.append('price', price);
            formData.append('category', category);
            formData.append('brand', brand);
            formData.append('company',company);
            formData.append('detail', detail);
            formData.append('status', status);
            formData.append('sale', sale);

            Object.keys(file).map((item, i) => {
                formData.append("file[]", file[item]);
                // console.log(file[item])
            });
            Object.keys(avatarCheckBox).map((item, i) => {
                formData.append("avatarCheckBox[]", avatarCheckBox[item]);
            });
            axios.post(url, formData, config)
              .then(res => {
                if(res){
                  console.log(res)
                }
                else{
                  console.log(res.data.errors)
                }
              })
              .catch(error => console.log(error));
        }
    }

  	render () {
	   return (
        <div className="col-sm-9">
            <div className="col-sm-12" id="cart_items">
                <div className="signup-form">
                    <h2>Edit product!</h2>
                    <form encType="multipart/form-data" onSubmit={this.formeditproduct}>
                                            
                        <input type="text" placeholder="Name" name="name" value={this.state.name} onChange={this.showvalue}/>
                        <input type="text" placeholder="Price" name="price" value={this.state.price} onChange={this.showvalue} />

                        <select name="category" value={this.state.category} onChange={this.showvalue}>
                            <option value="">Please choose category</option>
                            {this.showcategory()}
                        </select> 

                        <select name="brand" value={this.state.brand} >
                            <option value="">Please choose brand</option>
                            {this.showbrand()}
                        </select>
                            
                        <select value="" name="status" value={this.state.status} onChange={this.showvalue}>
                            <option value="0">Please choose status</option>
                            <option value="1">New</option>
                            <option value="2">Sale</option>
                        </select>    
                        {this.showsale()}
                        <textarea type="text" placeholder="Detail" name="detail" value={this.state.detail} onChange={this.showvalue}/>
                        <input type="text" placeholder="Company" name="company" value={this.state.company} onChange={this.showvalue}/>
                        <input type="file" multiple name="file" onChange={this.xulyfile}></input> 
                        <div className="imgPreview">
                            <ul>
                                {this.showimage()}
                            </ul>
                        </div>
                        <Err formErr={this.state.formErr} />
                        <button type="submit" className="btn btn-default">Update Product</button>
                    </form>
                    <br></br>
                </div>
            </div>
        </div>
    )
  }
}
export default Edit
