import React, { Component } from 'react'
import { Link } from "react-router-dom";
import axios from "axios";
class Delete extends Component {
    constructor(props) {
        super(props)
	    	this.state = {
          account : JSON.parse(localStorage["a"])
        }
        this.Deleteproduct = this.Deleteproduct.bind(this)

    }
    Deleteproduct(){
      let account = this.state.account
  		let accessToken = account.ob['success']['token'] 
      let config = {                
        headers: {                
        'Authorization': 'Bearer '+ accessToken,                
        'Content-Type': 'application/x-www-form-urlencoded',                
        'Accept': 'application/json'                
        }                
      }; 
      let url='http://localhost/laravel/laravel/public/api/user/delete-product/' + this.props.id   
      console.log(config)
      axios.get(url, config)
        .then(res => {           
            console.log(res.data.data)
            this.props.removeproduct(res.data.data)
      })
      .catch(error => console.log(error));
      
    }
  	render () {
	    return (
        <Link className="cart_quantity_remove" onClick={this.Deleteproduct} ><i className="fa fa-times" /></Link>
      )
  }
}
export default Delete
