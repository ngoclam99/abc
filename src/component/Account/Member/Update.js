import React, { Component } from "react";
import axios from "axios";
import Err from "./Err";
import { withRouter } from 'react-router';
class Update extends Component{
    constructor(props){
      super(props)
      this.state = {
        name : '',
        email : '',
        pass : '',
        phone :'',
        add : '',
        avatar:'',
        formErr: {},
        file : '',
        user:[]
      }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.formupdate = this.formupdate.bind(this)
    this.showname = this.showname.bind(this)
    this.showpass = this.showpass.bind(this)
    this.showphone = this.showphone.bind(this)
    this.showadd = this.showadd.bind(this)
    this.xulyfile = this.xulyfile.bind(this)
  }
componentDidMount(){
    var getlocal = localStorage.getItem("a")
    if(getlocal) {
        var acc=JSON.parse(getlocal);
            if (acc.islogin != true){
              this.props.history.push('/login')
              }
            else{
              let account = acc.ob.Auth
              this.setState({
                user: acc.ob
              })
              this.setState({name: account.name})
              this.setState({email: account.email})
              this.setState({pass: account.pass})
              this.setState({phone: account.phone})
              this.setState({add: account.address})
              this.setState({avatar: account.avatar})
            }
          }
}
showname(e){
  this.setState({
    name: e.target.value
  })
}
showpass(e){
  this.setState({
    pass: e.target.value
  })
}
showphone(e){
  this.setState({
    phone: e.target.value
  })
}
showadd(e){
  this.setState({
    add: e.target.value
  })
}
xulyfile(e){
  let file = e.target.files
  let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        avatar : e.target.result,
        file : file[0]
      })
    }
    reader.readAsDataURL(file[0])
  
}
formupdate(e){
  e.preventDefault();
  let name = this.state.name
  let email = this.state.email
  let pass = this.state.pass
  let phone = this.state.phone
  let add = this.state.add
  let file = this.state.file
  let avatar = this.state.avatar
  let err =this.state.formErr;
  let flag=true

  err.name = ""
  err.email= ""
  err.pass = ""
  err.file =""
  err.phone = ""
  err.add = ""
  const kt = /\S+@\S+\.\S+/;
  const dt = /^\d{10,11}$/
  let img =['png', 'jpg', 'jpeg', 'PNG', 'JPG']
  if (!name) {
    flag=false;
    err.name="Nhap ten";
  }

  if (email=="") {
    flag=false;
    err.email="Nhap email";
  }
  else{
    if (!kt.test(String(email))) {
      flag=false;
      err.email= "Sai dinh dang email"
    }
  }
  if (!add) {
    flag=false;
    err.add="Nhap dia chi";
  }

  if (!phone) {
    flag=false;
    err.phone="Nhap sdt";
  }
  else{
    if(!dt.test(phone)){
      flag= false;
      err.phone ="so dien thoai co 10 so"
    }
  }

  if(file && file.name !== ''){
    let abc = file['name'].split(".");

    if(file.size > 1024 * 1024) {
      flag = false;
      err.file = "dung luong qua lon"
    } else if(!img.includes(abc[1])) {
      flag = false;
      err.file = "khong dung dinh dang hinh anh"
    }
  }

  if(!flag){
    this.setState({
      formErr : err
    });
  }
  else{
    this.setState({
      formErr : {}
    });

  let  user = this.state.user
  let accessToken = user['success']['token'] 
  let config = {                
    headers: {                
    'Authorization': 'Bearer '+ accessToken,                
    'Content-Type': 'application/x-www-form-urlencoded',                
    'Accept': 'application/json'                
    }                
    };
    let url='http://localhost/laravel/laravel/public/api/user/update/'+user["Auth"]["id"]
    const formData = new FormData();                
    formData.append('name', name); 
    formData.append('email', email);
    formData.append('password', pass);
    formData.append('phone', phone);
    formData.append('address', add);
    formData.append('password', pass);
    formData.append('avatar', avatar);
    formData.append('level', 0);
    axios.post(url, formData, config)
    .then(res => {
      if(res){
        console.log(res.data)
        localStorage.clear();
        let islogin=true
        let ob = res.data
        let object ={
          islogin,
          ob
          }
        localStorage.setItem("a", JSON.stringify(object))
      }
      else{
        console.log(res.data.errors)
      }
    })
    .catch(error => console.log(error));
  }

  
}
render(){
  return(
    <>
    <div className="col-sm-2"></div>
    <div className="col-sm-5">
      <div className="signup-form">{/*sign up form*/}
        <h2> User Update!</h2>
        <form encType="multipart/form-data" onSubmit={this.formupdate}>
                                  
            <input type="text"  value={this.state.name} onChange={this.showname}/>
            <input type="email"  readonly  value={this.state.email} />
            <input type="password" value={this.state.pass} onChange={this.showpass}  />
            <input type="text" value={this.state.phone} onChange={this.showphone} />
            <input type="text" value={this.state.add} onChange={this.showadd} />
            <input type=""value="0" />
            <input type="file"  onChange={this.xulyfile}></input> {/*files={"http://localhost/laravel/laravel/public/upload/user/avatar/"+this.state.avatar}*/}
            <Err formErr={this.state.formErr} />
            <button type="submit" className="btn btn-default">Update</button>
            </form>
      </div>{/*/sign up form*/}
      <br></br>
    </div>
    </>
  );
  }
    
}
export default withRouter(Update);