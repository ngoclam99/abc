import React, { Component } from 'react';
import { withRouter } from 'react-router';
import Footer from '../Layout/Footer';
import Header from '../Layout/Header';
import Leftmenu from './Leftmenu';

class App extends Component {
  render () {
    return (
      <div className="container">
        {<Leftmenu />}
        {this.props.children}
      </div>
    )
  }
}
export default withRouter(App)