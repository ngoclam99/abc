import React from 'react';
import { Component } from 'react';
import { withRouter } from 'react-router';
import './App.css';
import Header from './component/Layout/Header';
import Footer from './component/Layout/Footer';
import { AppContext } from './component/Layout/AppContext';
class App extends Component {
  constructor (props){
    super(props)
    this.state = {
      checklogin : false
    }
    this.stateLoginContext = this.stateLoginContext.bind(this)
    this.stateCartContext = this.stateCartContext.bind(this)
  }
  stateLoginContext(flag) {
    localStorage["islogin"] = JSON.stringify(flag);
  }
  stateCartContext(cartmenu){
    localStorage["cartmenu"] = JSON.stringify(cartmenu);
  }
  render(){
    return (
      <AppContext.Provider value={{
        state: this.state,
        loginContext: this.stateLoginContext,
        stateCartContext: this.stateCartContext
      }}>
        <div className="App">
          <Header />
          {this.props.children}
          <Footer />

        </div>
      </AppContext.Provider>
    );
  }
}

export default withRouter(App);