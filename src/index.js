import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  userRouteMatch
} from "react-router-dom"
import './index.css';
import App from './App';
import Home from './component/Layout/Home';
import List from './component/Blog/List';
import Login from './component/Layout/Login'
import Account from './component/Account/Index'
import reportWebVitals from './reportWebVitals';
import Detail from './component/Blog/Detail';
import Detailproduct from './component/Layout/Detailproduct';
import Cart from './component/Layout/Cart';
import Bloglist from './component/Blog/Bloglist';
import Detail2 from './component/Blog/Detail2';
ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App>
        <Switch>
          <Route exact path='/' component={Home} />

          {/* <Route path='/blog/list' component={List} /> */}
          <Route path='/blog/list' component={Bloglist} />

          {/* <Route path='/blog/detail/:id' component={Detail} /> */}
          <Route path='/blog/detail/:id' component={Detail2} />


          <Route path='/login' component={Login} />
          <Route path='/product/detail/:id' component={Detailproduct} />
          <Route path='/product/cart' component={Cart} />
          <Route component={Account} />
          

        </Switch>
      </App>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
